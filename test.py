from locust import HttpLocust, TaskSet, task
from locust import events
from bs4 import BeautifulSoup
import random
from requests.auth import HTTPBasicAuth

def is_static_file(f):
    if "http" in f:
        if "/files" in f:
            return True
        elif "/styles" in f:
            return True
        elif f.endswith(".png"):
            return True
        elif f.endswith(".jpg"):
            return True
    else:
        return False

def fetch_static_assets(session, response):
    resource_urls = set()
    soup = BeautifulSoup(response.text, "html.parser")

    for res in soup.find_all(src=True):
        url = res['src']
        if is_static_file(url):
            resource_urls.add(url)
        else:
            print "Skipping: " + url
    for url in set(resource_urls):
        if "amazonaws.com" in url:
            session.client.get(url, name="(S3 Request)")
            print "S3: " + url
        elif "/s3/files/styles" in url:
            session.client.get(url, name="(S3 Generation Request)", auth=session.myAuth)
            print "Generation: " + url
        else:
            session.client.get(url, name="(Static File)", auth=session.myAuth)
            print "Regular: " + url
   

class AnonBrowsingUser(TaskSet):
    areas = [ "Sydney, New South Wales, Australia", "Australian+Capital+Territory,+Australia", "Tasmania,+Australia", "Fitzroy,+Victoria,+Australia" ] 
    myAuth = HTTPBasicAuth("pawshake", "pawshak3!")

    @task(10)
    def frontpage(l):
        response = l.client.get("/", auth=l.myAuth)
        fetch_static_assets(l, response)

    @task(20)
    def random_host(l):
        response = l.client.get("/devel/random/host", auth=l.myAuth)
        fetch_static_assets(l, response)
        print response.url, response.elapsed

    @task(20)
    def random_pet(l):
        response = l.client.get("/devel/random/pet", auth=l.myAuth)
        fetch_static_assets(l, response)

    @task(10)
    def search_area(l):
        response = l.client.get("/", name="Frontpage", auth=l.myAuth)
        response = l.client.post("/search.php", {"keys":random.choice(l.areas), "from":"", "to":"", "search_path":"petsitters", "language":"en-AU" }, auth=l.myAuth)
        fetch_static_assets(l, response)



class AuthBrowsingUser(TaskSet):
    areas = [ "Sydney, New South Wales, Australia", "Australian+Capital+Territory,+Australia", "Tasmania,+Australia", "Fitzroy,+Victoria,+Australia" ] 
    myAuth = HTTPBasicAuth("pawshake", "pawshak3!")

    def on_start(l):
        response = l.client.get("/user/login?destination=dashboard", name="Login")
        soup = BeautifulSoup(response.text, "html.parser")
        form_id = soup.select('input[name="form_build_id"]')[0]["value"]
        response = l.client.post("/user/login?destination=dashboard", {"name":"nnewton@tag1consulting.com", "pass":"namdev", "form_id":"user_login", "op":"Log+in", "address":"", "form_build_id":form_id}, auth=l.myAuth)

    @task(10)
    def frontpage(l):
        response = l.client.get("/", name="Frontpage (Auth)")
        fetch_static_assets(l, response)

    @task(20)
    def random_host(l):
        response = l.client.get("/devel/random/host", name="Random Host (Auth)")
        fetch_static_assets(l, response)

    @task(20)
    def random_pet(l):
        response = l.client.get("/devel/random/pet", name="Random Pet (Auth)")
        fetch_static_assets(l, response)

    @task(10)
    def search_area(l):
        response = l.client.get("/", name="Frontpage")
        response = l.client.post("/search.php", {"keys":random.choice(l.areas), "from":"", "to":"", "search_path":"petsitters", "language":"en-AU" }, auth=l.myAuth)
        fetch_static_assets(l, response)


class WebsiteAuthUser(HttpLocust):
    task_set = AuthBrowsingUser    
    min_wait=50000
    max_wait=60000

#class WebsiteAnonUser(HttpLocust):
#    task_set = AnonBrowsingUser
#    min_wait=50000
#    max_wait=60000
